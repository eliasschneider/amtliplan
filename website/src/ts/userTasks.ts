import { onMount } from "svelte";
import { writable } from "svelte/store";
import { amp, browser, dev, mode, prerendering } from "$app/env";
import toast from "../components/toast";
let session_token;

if (browser) {
  session_token = "Bearer " + localStorage.getItem("session_token");
}

/** List the tasks
 
 Permissions: `userTask_write`
 @param query Takes `startDate` and `endDate` as query parameters
 */
async function get(query: string) {
  var requestOptions = {
    method: "GET",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
  };

  return await (
    await fetch(
      `${import.meta.env.VITE_API_URL}/userTask/` + query,
      requestOptions
    )
  ).json();
}

/** Create a new user task
 * 
 *Permissions: `userTask_write`
 @param user_id User that the task is assigned to
 @param user_id_dep Deputy user that the task is assigned to
 @param date Date of the task
 @param task_id The task id
 @param created_by UID of the user that created the task
 */
async function create(data) {
  var requestOptions = {
    method: "POST",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
    body: JSON.stringify(data),
  };

  let res = await fetch(
    `${import.meta.env.VITE_API_URL}/userTask/`,
    requestOptions
  );
  if (res.ok) {
    window.location.reload();
  } else {
    toast.error(await res.text());
  }
}

/** Change the status of a user task 
 @param task_id The task id
*/
async function changeStatus(task_id: number, reload = true) {
  var requestOptions = {
    method: "PATCH",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
  };

  let res = await fetch(
    `${import.meta.env.VITE_API_URL}/userTask/${task_id}/changeStatus`,
    requestOptions
  );
  if (res.ok) {
    if (reload) window.location.reload();
    return res;
  } else {
    toast.error(await res.text());
  }
}

/** Delete a task

 Permissions: `userTask_write`
 @param task_id The task id
 */
async function deleteTask(task_id: number) {
  var requestOptions = {
    method: "DELETE",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
  };

  let res = await fetch(
    `${import.meta.env.VITE_API_URL}/userTask/${task_id}/`,
    requestOptions
  );
  if (res.ok) {
    toast.success("Erfolgreich gelöscht.");
  } else {
    toast.error(await res.text());
  }
}

/** Get the userTasks that weren't done

 Permissions: `user.write` or own `uid`
 @param user_id The user id
 */
async function getUndoneTasks(task_id: number) {
  var requestOptions = {
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
  };

  let res = await fetch(
    `${import.meta.env.VITE_API_URL}/userTask/undoneTasks/${task_id}/`,
    requestOptions
  );
  if (res.ok) {
    return await res.json();
  } else {
    toast.error(await res.text());
  }
}

export default {
  get,
  create,
  changeStatus,
  deleteTask,
  getUndoneTasks,
};
