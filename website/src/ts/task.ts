import { onMount } from "svelte";
import { writable } from "svelte/store";
import { amp, browser, dev, mode, prerendering } from "$app/env";
import toast from "../components/toast";
import { timeToDate } from "./global";
let session_token;

// Get the session token from local storage
if (browser) {
  session_token = "Bearer " + localStorage.getItem("session_token");
}

async function get(query = "") {
  var requestOptions = {
    method: "GET",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
  };

  return await (
    await fetch(`${import.meta.env.VITE_API_URL}/task/${query}`, requestOptions)
  ).json();
}

async function update(taskObject, task_id) {
  taskObject.deadline_time = timeToDate(taskObject.deadline_time);
  var requestOptions = {
    method: "PATCH",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
    body: JSON.stringify(taskObject),
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/task/${task_id}`,
    requestOptions
  );
  if (result.ok) {
    window.location.reload();
  } else {
    toast.error(await result.text());
  }
}

async function create(taskObject) {
  taskObject.deadline_time = timeToDate(taskObject.deadline_time);
  var requestOptions = {
    method: "POST",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],

    body: JSON.stringify(taskObject),
  };

  let res = await fetch(`${import.meta.env.VITE_API_URL}/task`, requestOptions);
  if (res.ok) {
    window.location.reload();
  } else {
    toast.error(await res.text());
  }
}

async function deleteTask(task_id) {
  var requestOptions = {
    method: "DELETE",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
  };

  let res = await fetch(
    `${import.meta.env.VITE_API_URL}/task/${task_id}`,
    requestOptions
  );
  if (res.ok) {
    window.location.reload();
  } else {
    toast.error(await res.text());
  }
}

export default {
  get,
  update,
  deleteTask,
  create,
};
