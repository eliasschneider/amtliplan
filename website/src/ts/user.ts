import { onMount } from "svelte";
import { writable } from "svelte/store";
import { amp, browser, dev, mode, prerendering } from "$app/env";
import toast from "../components/toast";
let session_token;

// Get the session token from local storage
if (browser) {
  session_token = "Bearer " + localStorage.getItem("session_token");
}

/** get the user */
async function get() {
  var requestOptions = {
    method: "GET",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
  };
  let result =
    JSON.parse(sessionStorage.getItem("user")) ??
    (await (
      await fetch(`${import.meta.env.VITE_API_URL}/user`, requestOptions)
    ).json()); // try  to get the user from session storage else make the request
  sessionStorage.setItem("user", JSON.stringify(result)); // Save the user to session storage to prevent too many requests
  return result;
}

/** list all users */
async function list(showHiddenUsers = false) {
  var requestOptions = {
    method: "GET",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
  };

  let res = await fetch(
    `${
      import.meta.env.VITE_API_URL
    }/user/list?showHiddenUsers=${showHiddenUsers}`,
    requestOptions
  );

  if (res.ok) {
    return await res.json();
  } else {
    toast.error(await res.text());
  }
}

/** Change the password of the user
 @param oldPassword the old password
 @param newPassword the new password
 */
async function changePassword(oldPassword: string, newPassword: string) {
  var requestOptions = {
    method: "POST",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
    body: JSON.stringify({
      oldPassword: oldPassword,
      newPassword: newPassword,
    }),
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/user/changePassword`,
    requestOptions
  );

  if (result.ok) {
    toast.success("Passwort geändert");
  } else if (result.status == 401) {
    toast.error(await result.text());
  } else {
    toast.error("Passwort konnte nicht geändert werden");
  }
}

/** Update a user

 Permissions: `user_write`
 @param userObject The object with the new user data
 @param user_id The user_id of the user
 */
async function updateUser(userObject, user_id: number) {
  var requestOptions = {
    method: "PATCH",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
    body: JSON.stringify(userObject),
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/user/${user_id}`,
    requestOptions
  );

  if (result.ok) {
    toast.success("Benutzer wurde aktualisiert.");
    window.location.reload();
  } else {
    toast.error(await result.text());
  }
}

/** Create a new user
 * 
 Permissions: `user_write`
 @param userObject The object with the new user data
 */
async function signUp(user) {
  var requestOptions = {
    method: "POST",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
    body: JSON.stringify({
      email: user.email.toLowerCase(),
      first_name: user.first_name,
      last_name: user.last_name,
      password: user.password,
      roleId: user.roleId,
    }),
  };

  fetch(`${import.meta.env.VITE_API_URL}/user/signUp`, requestOptions).then(
    async (r) => {
      if (r.status == 201) {
        toast.success("Die Registrierung war erfolgreich");
      } else {
        toast.error(await r.text());
      }
    }
  );
}

/** Reset the password of an user
 
 Permissions: `user_write`
 */
async function resetPassword(newPassword: string, user_id: number) {
  var requestOptions = {
    method: "POST",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
    body: JSON.stringify({
      user_id: user_id,
      newPassword: newPassword,
    }),
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/user/resetPassword`,
    requestOptions
  );

  if (result.ok) {
    toast.success("Das Passwort wurde geändert.");
  } else {
    toast.error(await result.text());
  }
}

/** Delete a user
 
 Permissions: `user_write`
 @param user_id The user_id of the user
 */
async function deleteUser(user_id: number) {
  var requestOptions = {
    method: "DELETE",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/user/${user_id}`,
    requestOptions
  );
  if (result.ok) {
    window.location.reload();
  } else {
    toast.error(await result.text());
  }
}

/** Delete the session */
async function signOut() {
  var requestOptions = {
    method: "POST",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
  };
  await fetch(`${import.meta.env.VITE_API_URL}/user/signOut`, requestOptions);
  localStorage.clear();
  sessionStorage.clear();
  window.location.reload();
}

/** List all roles
  
 Permissions: `user.read`
 */
async function listRoles() {
  var requestOptions = {
    method: "GET",
    headers: [
      ["Content-Type", "application/json"],
      ["Authorization", session_token],
    ],
  };

  return await (
    await fetch(`${import.meta.env.VITE_API_URL}/role`, requestOptions)
  ).json();
}

export default {
  get,
  list,
  changePassword,
  signOut,
  updateUser,
  resetPassword,
  signUp,
  deleteUser,
  listRoles,
};
