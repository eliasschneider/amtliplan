import express from "express";
import checkAuthorization from "../middlewares/checkAuthorization.middleware";
import user from "../controller/user.controller";
import rateLimit from "../middlewares/rateLimit.middleware";
const router = express.Router();

router.post("/signIn", rateLimit.custom(100, 60), user.signIn);

router.get("/", user.get);

// router.get("/:user_id", checkAuthorization("user_write"), user.get);

router.post("/signUp", checkAuthorization("user_write"), user.create);

router.patch("/:user_id", checkAuthorization("user_write"), user.update);

router.post(
  "/resetPassword",
  checkAuthorization("user_write"),
  user.resetPassword
);

router.post("/changePassword", rateLimit.custom(30, 60), user.changePassword);

router.get(
  "/list",
  checkAuthorization(["user_write", "userTask_write"]),
  user.list
);

router.delete("/:user_id", checkAuthorization("user_write"), user.remove);

router.post("/signOut", user.signOut);

export default router;
