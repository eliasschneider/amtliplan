import crypto from "crypto";
import { prisma } from "./prisma.util";

function hashPW(
  password: crypto.BinaryLike,
  salt = crypto.randomBytes(16).toString("hex")
) {
  // Hashing user's salt and password with 1000 iterations,

  const hash = crypto
    .pbkdf2Sync(password, salt, 1000, 64, "sha512")
    .toString("hex");

  return { hash, salt };
}

async function createSession(user_id: number) {
  const session_token = crypto.randomBytes(122).toString("hex");
  await prisma.user_sessions.create({
    data: {
      session_token: session_token,
      id_user: user_id,
    },
  });
  return session_token;
}

async function validateSession(session_token: string) {
  try {
    session_token = session_token.replace("Bearer ", "");

    const result = await prisma.user_sessions.findFirst({
      where: {
        session_token: session_token,
        expire_at: { gt: new Date(Date.now()) },
      },
    });

    if (!result && !session_token) {
      return true;
    } else {
      return false;
    }
  } catch (_) {
    return false;
  }
}

async function getUserBySession(session_token: string) {
  session_token = session_token.replace("Bearer ", "");
  const result = await prisma.user_sessions.findUnique({
    select: {
      id_user: true,
    },
    where: {
      session_token: session_token,
    },
  });
  if (!result) throw "Invalid or no session token";
  return result.id_user;
}

async function checkPermission(session_token: string, scopes: Array<string>) {
  let permissionOk = false;
  try {
    const user_id = await getUserBySession(session_token);
    for (const scope of scopes) {
      const scopeFilter = JSON.parse(`{"${scope}": true}`);

      const result = await prisma.users.findFirst({
        select: {
          role: {
            select: {
              permissions: true,
            },
          },
        },
        where: {
          AND: [
            { user_id: user_id },
            {
              role: {
                permissions: scopeFilter,
              },
            },
          ],
        },
      });

      if (result != null) {
        permissionOk = true;
        return true;
      }
    }
    if (!permissionOk) return false;
  } catch (_) {
    console.log(_);
    return false;
  }
}

async function deleteSessions(user_id: number) {
  try {
    await prisma.user_sessions.deleteMany({
      where: {
        id_user: user_id,
      },
    });
    return true;
  } catch {
    return false;
  }
}

export default module.exports = {
  hashPW: hashPW,
  createSession: createSession,
  validateSession: validateSession,
  getUserBySession,
  checkPermission,
  deleteSessions,
};
