import express from "express";
import dotenv from "dotenv";
import rateLimit from "./middlewares/rateLimit.middleware";
import bodyParser from "body-parser";
import cors from "cors";
import jobs from "./jobs/jobs";

import role_route from "./routes/role.route";
import task_route from "./routes/task.route";
import user_route from "./routes/user.route";
import userTask_route from "./routes/userTask.route";

dotenv.config();
jobs();
const app = express();

app.use(
  cors({ allowedHeaders: ["authorization", "content-type"], origin: "*" })
);

app.use(bodyParser.json());

app.use(rateLimit.global);

app.use("/role", role_route);

app.use("/task", task_route);

app.use("/user", user_route);

app.use("/userTask", userTask_route);

app.listen(8080, () => {
  console.info("The api is listening at http://localhost:8080");
});
