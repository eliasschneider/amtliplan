import { NextFunction, Request, Response } from "express";
import authUtil from "../utils/auth.util";

const checkAuthorization =
  (scopes: Array<string> | string) =>
  async (req: Request, res: Response, next: NextFunction) => {
    const token = (req.get("Authorization") ?? "").replace("Bearer ", "");

    if (typeof scopes == "string") scopes = [scopes.toString()];

    if (scopes.includes("same_userid")) {
      const user_id = await authUtil.getUserBySession(token);

      if (parseInt(req.params.user_id) == user_id) {
        return next();
      } else {
        return res
          .status(403)
          .send("Du hast nicht die benötigten Berechtigungen.");
      }
    }

    if (await authUtil.checkPermission(token, scopes)) {
      next();
    } else {
      res.status(403).send("Du hast nicht die benötigten Berechtigungen.");
    }
  };

export default checkAuthorization;
