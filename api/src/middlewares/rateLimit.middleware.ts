import rateLimit from "express-rate-limit";

function custom(maxRequests: number, time: number) {
  return rateLimit({
    windowMs: time * 60 * 1000, // 1 hour
    max: maxRequests, // Limit each IP to 5 create account requests per `window` (here, per hour)
    message: "Zu viele Requests. Bitte versuche es später erneut.",
    standardHeaders: true,
    legacyHeaders: false,
  });
}

const global = rateLimit({
  windowMs: 10 * 60 * 1000,
  max: 500,
  standardHeaders: true,
  message: "Zu viele Anfragen von dieser IP. Versuche es in 15 Minuten erneut.",
  legacyHeaders: false,
});

export default {
  custom,
  global,
};
