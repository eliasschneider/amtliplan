import cleanSessions from "./db/cleanSessions";

export default function () {
  cleanSessions();
}
