import schedule from "node-schedule";
import { prisma } from "../../utils/prisma.util";

 const cleanSessions = ()=> schedule.scheduleJob("00 01 * * *", async function () {
  const result = await prisma.user_sessions.deleteMany({
    where: {
      expire_at: { lt: new Date() },
    },
  });

  console.log(`[SCHEDULE] Deleted ${result.count} user sessions.`);
});

export default cleanSessions;