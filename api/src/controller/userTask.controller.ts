import { Prisma } from "@prisma/client";
import { Request, Response } from "express";
import authUtil from "../utils/auth.util";
import userTask from "../service/userTask.service";

async function create(req: Request, res: Response) {
  const data = req.body;

  try {
    const result = await userTask.create(data);
    res.status(201).send(result);
  } catch (_) {
    console.log(_);
    res.status(400).send("Fehler beim Erstellen eines Benutzerämtlis.");
  }
}

async function update(req: Request, res: Response) {
  const data = req.body;
  const user_task_id = parseInt(req.params.user_task_id);
  try {
    await userTask.update(user_task_id, data);
    res.status(200).send(await userTask.get(user_task_id));
  } catch (_) {
    res.status(400).send("Fehler beim Aktualisieren eines Benutzerämtlis.");
  }
}

async function remove(req: Request, res: Response) {
  const user_task_id = parseInt(req.params.user_task_id);
  try {
    await userTask.remove(user_task_id);
    res.sendStatus(200);
  } catch (_) {
    res.status(400).send("Fehler beim Löschen eines Benutzerämtlis.");
  }
}

async function list(req: Request, res: Response) {
  // TODO: Update the filter, it's solved really dirty
  try {
    const filters: Prisma.user_tasksWhereInput = {};
    let sortOrder: Prisma.SortOrder = "asc";

    if (!isNaN(parseInt(req.query.user_id as string))) {
      filters.id_user = parseInt(req.query.user_id as string);
    }

    if (req.query.endDate != undefined || req.query.endDate != undefined) {
      filters.AND = [
        {
          date: {
            gte: new Date((req.query.startDate as string) ?? "1000-01-01"),
          },
        },
        {
          date: {
            lte: new Date((req.query.endDate as string) ?? "2999-01-01"),
          },
        },
      ];
    }
    if (req.query.sortOrder != undefined) {
      sortOrder = req.query.sortOrder as Prisma.SortOrder;
    }

    if (req.query.done != undefined) {
      filters.isDone = JSON.parse(req.query.done as string);
    }

    res.status(200).send(await userTask.list(filters, sortOrder));
  } catch (_) {
    console.log(_);
    res.send(500);
  }
}

async function changeStatus(req: Request, res: Response) {
  const task_id = parseInt(req.params.task_id);
  const { status } = req.body;
  const session_token = req.get("Authorization").replace("Bearer ", "");

  try {
    if (userTask.get(task_id) == null) {
      return res.status(404).send("Dieses Ämtli existiert nicht.");
    }

    res
      .status(200)
      .send(await userTask.changeStatus(task_id, status, session_token));
  } catch (_) {
    console.log(_);
    res.status(400).send("Fehler beim ändern des Statuses.");
  }
}

async function getUndoneTasks(req: Request, res: Response) {
  try {
    const user_id = await authUtil.getUserBySession(
      req.get("Authorization").replace("Bearer ", "")
    );

    res.status(200).send(await userTask.getUndoneTasks(user_id));
  } catch (_) {
    console.log(_);
    res.status(400).send("Fehler beim ändern des Statuses.");
  }
}

export default module.exports = {
  create,
  list,
  update,
  remove,
  changeStatus,
  getUndoneTasks,
};
