import { Request, Response } from "express";
import user from "../service/user.service";

async function create(req: Request, res: Response) {
  const data = req.body;
  try {
    const result = await user.create(data);

    res.status(201).send(await user.get(result.user_id));
  } catch (_) {
    res.status(400).send("Failed to create user.");
  }
}

async function signIn(req: Request, res: Response) {
  const { email } = req.body;
  const { password } = req.body;

  try {
    res.status(200).send(await user.signIn(email, password));
  } catch {
    res.status(401).send("Falsche Email oder Passwort.");
  }
}

async function update(req: Request, res: Response) {
  const user_id = parseInt(req.params.user_id);
  const data = req.body;
  try {
    await user.update(user_id, data);
    res.status(200).send(await user.get(user_id));
  } catch (_) {
    console.log(_);
    res.status(400).send("Fehler beim aktualisieren eines Benutzers.");
  }
}
async function remove(req: Request, res: Response) {
  const user_id = parseInt(req.params.user_id);
  try {
    await user.remove(user_id);
    res.sendStatus(204);
  } catch (_) {
    res.status(400).send("Fehler beim löschen eines Benutzers.");
  }
}

async function changePassword(req: Request, res: Response) {
  const session_token = req.get("Authorization").replace("Bearer ", "");
  const { oldPassword } = req.body;
  const { newPassword } = req.body;
  try {
    await user.changePassword(session_token, oldPassword, newPassword);
    res.send("Das Passwort wurde erfolgreich geändert.");
  } catch {
    res.status(401).send("Falsches Passwort.");
  }
}

async function resetPassword(req: Request, res: Response) {
  const { user_id } = req.body;
  const { newPassword } = req.body;
  try {
    await user.resetPassword(user_id, newPassword);
    res.send("Password erfolgreich geändert.");
  } catch (e) {
    res.sendStatus(500);
  }
}

async function list(req: Request, res: Response) {
  const showHiddenUsers = JSON.parse(
    (req.query.showHiddenUsers as string).toLowerCase()
  );
  try {
    res.status(200).send(await user.list(showHiddenUsers));
  } catch (_) {
    console.log(_);
    res.sendStatus(500);
  }
}
async function get(req: Request, res: Response) {
  const session_token = req.get("Authorization").replace("Bearer ", "");

  const user_id = parseInt(req.params.user_id);

  try {
    if (isNaN(user_id)) {
      res.status(200).send(await user.getBySessionToken(session_token));
    } else {
      res.status(200).send(await user.get(user_id));
    }
  } catch (_) {
    console.log(_);
    res.sendStatus(500);
  }
}

async function signOut(req: Request, res: Response) {
  const session_token = req.get("Authorization").replace("Bearer ", "");
  try {
    await user.signOut(session_token);
    res.sendStatus(204);
  } catch {
    res.sendStatus(401);
  }
}

export default module.exports = {
  signIn,
  create,
  get,
  list,
  changePassword,
  resetPassword,
  update,
  remove,
  signOut,
};
