import { prisma } from "../utils/prisma.util";
import { Prisma } from "@prisma/client";

async function create(data: Prisma.tasksCreateInput) {
  return await prisma.tasks.create({
    data: data,
  });
}

async function update(task_id: number, data: Prisma.tasksUpdateInput) {
  return await prisma.tasks.update({
    data: data,
    where: {
      task_id: task_id,
    },
  });
}

async function remove(task_id: number) {
  prisma.tasks.delete({
    where: {
      task_id: task_id,
    },
  });
}

async function list() {
  return await prisma.tasks.findMany({ take: 50 });
}

async function get(task_id: number) {
  return await prisma.tasks.findUnique({
    where: {
      task_id: task_id,
    },
  });
}

export default module.exports = {
  create,
  list,
  update,
  remove,
  get,
};
