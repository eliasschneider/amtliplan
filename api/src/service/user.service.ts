import auth from "../utils/auth.util";
import { prisma } from "../utils/prisma.util";
import { Prisma } from "@prisma/client";

async function create(data: Prisma.usersCreateInput) {
  const hashedPW = auth.hashPW(data.password);
  data.password = hashedPW.hash;
  data.salt = hashedPW.salt;

  return await prisma.users.create({
    data: data,
  });
}

async function checkCredentials(email: string, password: string) {
  try {
    const { salt } = await prisma.users.findUnique({
      where: {
        email: email,
      },
    });

    const hashedPW = auth.hashPW(password, salt);
    const user = await prisma.users.findFirst({
      where: {
        AND: {
          email: email,
          password: hashedPW.hash,
        },
      },
    });
    if (user != null) {
      return { user_id: user.user_id, isValid: true };
    } else {
      return { user_id: user.user_id, isValid: false };
    }
  } catch (_) {
    return { isValid: false };
  }
}

async function signIn(email: string, password: string) {
  const credentials = await checkCredentials(email, password);

  if (credentials.isValid) {
    return await auth.createSession(credentials.user_id);
  } else {
    throw "Invalid credentials";
  }
}

async function update(user_id: number, data: any) {
  data.role = undefined;
  return await prisma.users.update({
    data: data,
    where: {
      user_id: user_id,
    },
  });
}
// TODO: Check user delete constraint
async function remove(user_id: number) {
  return await prisma.users.delete({
    where: {
      user_id: user_id,
    },
  });
}

async function changePassword(
  session_token: string,
  oldPassword: string,
  newPassword: string
) {
  const user = await prisma.users.findUnique({
    where: {
      user_id: await auth.getUserBySession(session_token),
    },
  });

  const credentials = await checkCredentials(user.email, oldPassword);

  if (credentials.user_id != user.user_id) {
    throw "Unauthorzized";
  } else if (credentials.isValid) {
    resetPassword(user.user_id, newPassword);
    return await auth.deleteSessions(user.user_id);
  }
}

async function resetPassword(user_id: number, newPassword: string) {
  const newHashedPW = auth.hashPW(newPassword);

  return await prisma.users.update({
    data: {
      password: newHashedPW.hash,
      salt: newHashedPW.salt,
    },
    where: {
      user_id: user_id,
    },
  });
}

async function list(showHiddenUsers: boolean) {
  let filters = {};

  if (!showHiddenUsers) {
    filters = {
      ...filters,
      role: {
        isAssignable: true,
      },
    };
  }

  return await prisma.users.findMany({
    select: {
      user_id: true,
      first_name: true,
      last_name: true,
      email: true,
      role: {
        select: {
          user_role_id: true,
          name: true,
        },
      },
    },
    where: filters,
    orderBy: {
      first_name: "asc",
    },
    take: 100,
  });
}
async function getBySessionToken(session_token: string) {
  const user_id = await auth.getUserBySession(session_token);
  return await get(user_id);
}
async function get(user_id: number) {
  return await prisma.users.findUnique({
    select: {
      user_id: true,
      first_name: true,
      last_name: true,
      email: true,
      role: {
        select: {
          name: true,
          permissions: true,
        },
      },
    },
    where: {
      user_id: user_id,
    },
  });
}

async function signOut(session_token: string) {
  return await prisma.user_sessions.update({
    data: {
      expire_at: new Date(Date.now()),
    },
    where: {
      session_token: session_token,
    },
  });
}

export default module.exports = {
  signIn,
  create,
  get,
  getBySessionToken,
  list,
  changePassword,
  resetPassword,
  update,
  remove,
  signOut,
};
